Name:           mdtmftp
Version:        1.1.1
Release:        1%{?dist}
Summary:        An implementation of high peroformance FTP using MDTM middleware 

License:        GPL
URL:            https://mdtm.fnal.gov
Source0:        %{url}/dist/mdtm/%{name}-%{version}-%{release}.tar.gz

BuildRequires:  
Requires:       

%description


%prep
%autosetup


%build
%configure
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Wed Jun 23 2021 K. de Jong <keesdejong@fedoraproject.org>
- 
