Name:           mdtm
Version:        1.0.5
Release:        1%{?dist}
Summary:        Multicore-aware Data Transfer Middleware

License:        GPL
URL:            https://mdtm.fnal.gov
Source0:        %{url}/downloads/%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  gcc-c++
BuildRequires:  make
BuildRequires:  autoconf
BuildRequires:  automake
BuildRequires:  libtool
BuildRequires:  m4
BuildRequires:  numactl-devel
BuildRequires:  hwloc-devel
BuildRequires:  lvm2-devel
BuildRequires:  libblkid-devel
BuildRequires:  openssl-devel
BuildRequires:  mosquitto-devel
BuildRequires:  json-c-devel
BuildRequires:  krb5-devel
BuildRequires:  libtool-ltdl-devel
BuildRequires:  ncurses-devel
BuildRequires:  libuuid-devel
BuildRequires:  rrdtool-devel

Requires:       hwloc >= 1.5
Requires:       bitmask
Requires:       cpuset
Requires:       /etc/mime.types

%description


%prep
%autosetup


%build
%configure \
    --libdir=%{_libdir} \
    --sysconfdir=%{_sysconfdir} \
    --includedir=%{_includedir} \
    --libexecdir=%{_libdir}

%make_build


%install
%make_install


%files
%license add-license-file-here
%doc add-docs-here



%changelog
* Wed Jun 23 2021 K. de Jong <keesdejong@fedoraproject.org>
- 
