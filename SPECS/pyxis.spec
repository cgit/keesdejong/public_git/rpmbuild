%global debug_package %{nil}

Name:           pyxis
Version:        0.7.0
Release:        1%{?dist}
Summary:        Pyxis is a SPANK plugin for the SLURM workload manager

License:        Apache License 2.0
URL:            https://github.com/NVIDIA/%{name}/
Source0:        https://github.com/NVIDIA/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc
BuildRequires:  slurm-devel
Requires:       enroot >= 3.1.0

%description
Pyxis is a SPANK plugin for the SLURM Workload Manager. It allows unprivileged
cluster users to run containerized tasks through the srun command.

%prep
%setup -q
sed -i '/.*AUDIT_ARCH_AARCH64.*/d' seccomp_filter.h # seccomp_filter.h:28:38: error: 'AUDIT_ARCH_AARCH64' undeclared here (not in a function)

%build
make %{?_smp_mflags}

%install
make install prefix=%{_prefix} libdir=%{_libdir} DESTDIR=%{buildroot}

%files
%{!?_licensedir:%global license %%doc}
%license LICENSE
%doc README.md
%{_libdir}/slurm/*
%{_datadir}/%{name}/%{name}.conf

%changelog
* Wed Jul 15 2020 K. de Jong <keesdejong@fedoraproject.org> - 0.7.0-1
- new version

* Mon May 11 2020 K. de Jong <keesdejong@fedoraproject.org> - 0.6.0-1
- Initial package

