Name: enroot
Version: 3.1.0
Release: 1%{?dist}
License: ASL 2.0
URL: https://github.com/NVIDIA/%{name}/
Source0: https://github.com/NVIDIA/%{name}/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires: git-core gcc make libcap libtool automake

Summary: Unprivileged container sandboxing utility
#Conflicts: enroot
Requires: bash >= 4.2, curl, gawk, jq >= 1.5, parallel, shadow-utils, squashfs-tools
Requires: coreutils, grep, findutils, gzip, glibc-common, sed, tar, util-linux, zstd
#Recommends: pigz, ncurses
#Suggests: libnvidia-container-tools, squashfuse, fuse-overlayfs
%description
A simple yet powerful tool to turn traditional container/OS images into
unprivileged sandboxes.

This package provides the main utility, its set of helper binaries and
standard configuration files.

%prep
%setup -q

%build
%make_build prefix=%{_prefix} exec_prefix=%{_exec_prefix} libdir=%{_libdir} datarootdir=%{_datarootdir}

%install
%make_install prefix=%{_prefix} exec_prefix=%{_exec_prefix} libdir=%{_libdir} datarootdir=%{_datarootdir}

%files
%license LICENSE
%config(noreplace) %{_sysconfdir}/*
%{_libdir}/*
%{_bindir}/*
%{_datadir}/*

%package -n %{name}+caps
Summary: Unprivileged container sandboxing utility (extra capabilities)
Requires: %{name}%{?_isa} = %{version}-%{release}, libcap
%description -n %{name}+caps
A simple yet powerful tool to turn traditional container/OS images into
unprivileged sandboxes.

This dependency package grants extra capabilities to unprivileged users which
allows them to import and convert container images directly.

%changelog
* Tue Apr 28 2020 K. de Jong <keesdejong@fedoraproject.org> - 0.6.0-1
- rebuilt
