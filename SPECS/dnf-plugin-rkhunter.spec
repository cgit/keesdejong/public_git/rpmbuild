%{!?dnf_lowest_compatible: %global dnf_lowest_compatible 4.2.23}
%global srcname dnf-plugin-rkhunter

Name:           python-%{srcname}
Version:        1.1
Release:        1%{?dist}
Summary:        Automatic updates of rkhunter property files after DNF transactions

License:        GPLv2+
URL:            https://github.com/AquaL1te/%{srcname}
Source0:        https://github.com/AquaL1te/%{srcname}/archive/v%{version}.tar.gz#/%{srcname}-%{version}.tar.gz

Provides:       python3-%{srcname} = %{version}-%{release}
%{?python_provide:%python_provide python3-pkg_resources}

%global _description %{expand:
dnf-plugin-rkhunter is a plugin for DNF to do an automatic update of rkhunter
property files after each DNF transaction.}

%description %{_description}

%package -n python3-%{srcname}
Summary:        %{summary}
BuildArch:      noarch
BuildRequires:  python3-devel
Requires:       rkhunter

%description -n python3-%{srcname} %_description

%prep
%autosetup -n %{srcname}-%{version}

%install
mkdir -p %{buildroot}%{python3_sitelib}/dnf-plugins/
mkdir -p %{buildroot}%{_sysconfdir}/dnf/plugins/
mkdir -p %{buildroot}%{_defaultdocdir}/%{srcname}/
mkdir -p %{buildroot}%{_defaultlicensedir}/%{srcname}/
mkdir -p %{buildroot}%{_mandir}/man8/
install -m 644 plugin/rkhunter.py %{buildroot}/%{python3_sitelib}/dnf-plugins/rkhunter.py
install -m 644 etc/rkhunter.conf %{buildroot}%{_sysconfdir}/dnf/plugins/rkhunter.conf
install -m 644 README.md %{buildroot}%{_defaultdocdir}/%{srcname}/README.md
install -m 644 LICENSE %{buildroot}%{_defaultlicensedir}/%{srcname}/LICENSE
install -m 644 man/dnf-plugin-rkhunter.8 %{buildroot}%{_mandir}/man8/%{srcname}.8

%files -n python3-%{srcname}
%{python3_sitelib}/dnf-plugins/rkhunter.py
%{python3_sitelib}/dnf-plugins/__pycache__/rkhunter.*
%{_sysconfdir}/dnf/plugins/rkhunter.conf
%{_defaultdocdir}/%{srcname}/README.md
%{_defaultlicensedir}/%{srcname}/LICENSE
%{_mandir}/man8/%{srcname}.8*

%changelog
* Wed Jul 22 2020 K. de Jong <keesdejong@fedoraproject.org> - 1.1-1
- Initial package
